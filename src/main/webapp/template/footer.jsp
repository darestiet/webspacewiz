<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<script type="text/javascript" src="<%= request.getContextPath() %>/webjars/bootstrap/5.1.3/js/bootstrap.bundle.min.js" ></script>
<script type="text/javascript" src="<%= request.getContextPath() %>/assets/htmx.js" ></script>
