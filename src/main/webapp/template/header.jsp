<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="de.jalin.webspacewiz.navbar"/>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Webspace Wizard">
    <meta name="author" content="Peter Hormanns">
    <title><fmt:message key="navbar.title"/></title>
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/webjars/skeleton-css/2.0.4/css/normalize.css" />
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/webjars/skeleton-css/2.0.4/css/skeleton.css" />
    <link rel="stylesheet" type="text/css" href="<%= request.getContextPath() %>/assets/style.css" />
</head>
