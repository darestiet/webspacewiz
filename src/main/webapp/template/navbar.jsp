<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="de.jalin.webspacewiz.navbar"/>

<nav>
  <div class="container">
    <div class="row">
  	  <img alt="Webspace Wizard" src="<c:url value="/assets/logo.svg"/>">
  	
      <a class="button" href="<c:url value="/"/>">Home</a>
      <a class="button" href="<c:url value="/domains.jsp"/>">Domains</a>
      <a class="button" href="<c:url value="/email.jsp"/>">E-Mail (alt)</a>
      <a class="button" href="<c:url value="/mailbox-list"/>">E-Mail</a>
      <a class="button" href="<c:url value="/websites.jsp"/>">Webseiten und -Anwendungen</a>
    </div>
  </div>
</nav>
