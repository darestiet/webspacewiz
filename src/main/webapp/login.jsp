<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="de.jalin.webspacewiz.login"/>
<!DOCTYPE html>
<html lang="de">
<jsp:include page="template/header.jsp"/>
<body>
  <c:url var="logourl" value="/assets/logo.svg" />
  <c:url var="signinaction" value="/login" />
  
  <div class="container">
  	<div class="row">
  	  <div class="twelve columns">
        <img src="${logourl}" alt="Logo Hostsharing eG" width="276" height="48">
        <h1 class="h3 mb-3 font-weight-normal"><fmt:message key="login.text"/></h1>
      </div>
  	</div>
    <form method="post" action="${signinaction}">
      <div class="row">
  	    <div class="six columns">
          <label for="inputUser"><fmt:message key="login.user"/></label>
          <input type="text" id="inputUser" name="user" placeholder="<fmt:message key="login.user"/>" required autofocus>
      </div>
  	</div>
      <div class="row">
  	    <div class="six columns">
        <label for="inputPassword"><fmt:message key="login.password"/></label>
        <input type="password" id="inputPassword" name ="password" placeholder="<fmt:message key="login.password"/>" required>
      </div>
  	</div>
      <div class="row">
  	    <div class="six columns">
        <button class="button-primary" type="submit"><fmt:message key="login.signin"/></button>
      </div>
  	</div>
  	<div class="row">
  	  <div class="twelve columns">
        <p class="u-full-width">&copy; 2021 Hostsharing eG</p>
      </div>
  	</div>
    </form>
  </div>
  <jsp:include page="template/footer.jsp"/>
</body>
</html>
