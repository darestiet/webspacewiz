<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="de.jalin.webspacewiz.welcome"/>
<!DOCTYPE html>
<html lang="de">
<jsp:include page="template/header.jsp"/>
<body>
    <jsp:include page="template/navbar.jsp"/>

    <!-- Page Content -->
    <div class="container">
    	<div class="row eight columns">
    		<h4>E-Mail Adressen und Postfächer</h4>
            <table class="u-full-width">
	            <thead>
				    <tr>
				      <th>E-Mail Adressen</th>
				      <th>Postfach</th>
				      <th>Kommentar</th>
				      <th><button hx-get="/mailbox-form/?op=new" hx-target="#mailbox-form">Neues Postfach</button></th>
				    </tr>
			    </thead>
			    <tbody>
			    <c:forEach items="${mailboxes}" var="mbox">
				    <tr>
				      <td>max.muster@example.com<br/>max@example.com</td>
				      <td>${mbox.name}</td>
				      <td>${mbox.comment}</td>
				      <td><button hx-get="/mailbox-form/${mbox.name}?op=upd" hx-target="#mailbox-form">Edit</button> <button hx-get="/mailbox-form/${mbox.name}?op=del" hx-target="#mailbox-form">Delete</button></td>
				    </tr>
			    </c:forEach>
				    <tr>
				      <td>max.muster@example.com<br/>max@example.com</td>
				      <td>xyz00-max</td>
				      <td>Max Muster</td>
				      <td><button hx-get="/mailbox-form/xyz00-max?op=upd" hx-target="#mailbox-form">Edit</button> <button hx-get="/mailbox-form/xyz00-max?op=del" hx-target="#mailbox-form">Delete</button></td>
				    </tr>
				    <tr>
				      <td>moni.muster@example.com<br/>moni@example.com</td>
				      <td>xyz00-moni</td>
				      <td>Monika Muster</td>
				      <td><button hx-get="/mailbox-form/xyz00-moni?op=upd" hx-target="#mailbox-form">Edit</button> <button hx-get="/mailbox-form/xyz00-moni?op=del" hx-target="#mailbox-form">Delete</button></td>
				    </tr>
			    </tbody>
			</table>
    	</div>
    	<div class="row four columns" id="mailbox-form">
    	</div>
    </div>
    
    <jsp:include page="template/footer.jsp"/>
</body>
</html>
