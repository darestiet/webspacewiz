<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setBundle basename="de.jalin.webspacewiz.welcome"/>
<!DOCTYPE html>
<html lang="de">
<jsp:include page="template/header.jsp"/>
<body>
    <jsp:include page="template/navbar.jsp"/>

    <!-- Page Content -->
    <div class="container">
        <h1><fmt:message key="welcome.title"/></h1>
        <p><fmt:message key="welcome.text"/></p>
        <a href="https://doc.hostsharing.net" target="_blank">Benutzerhandbuch</a>
        <h3><a href="https://www.hostsharing.net/ziele/digitale-souveraenitaet/">Digitale Souveränität</a></h3>
        <p>Mit der Hostsharing eG haben wir ein wirtschaftlich unabhängiges Unternehmen geschaffen, das ausschließlich in unserem Interesse tätig ist. Wir, die Mitglieder der Genossenschaft, sind an allen Entscheidungen beteiligt. Unsere Genossenschaft arbeitet nicht gewinnorientiert. Überschüsse investieren wir in Rücklagen und die Weiterentwicklung unserer Leistungen. Wir legen alle Kosten transparent auf die Verursacher um. Freie Software und offene Standards machen uns unabhängig von proprietären Technologien und den wirtschaftlichen Interessen anderer Unternehmen. Wir schützen unsere Daten. </p>
    </div>
    
    <jsp:include page="template/footer.jsp"/>
</body>
</html>
