package de.jalin.webspacewiz.ssh;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.sshd.client.SshClient;
import org.apache.sshd.client.channel.ChannelExec;
import org.apache.sshd.client.channel.ClientChannelEvent;
import org.apache.sshd.client.future.ConnectFuture;
import org.apache.sshd.client.session.ClientSession;

import de.jalin.webspacewiz.Log;
import de.jalin.webspacewiz.TemplateEngine;
import de.jalin.webspacewiz.WizException;

public class SshRemote implements AutoCloseable {

	static Log log = new Log(SshRemote.class.getCanonicalName());
	
	private final ClientSession session;

	public SshRemote(final String webspace, final String password) throws WizException {
		final SshClient client = SshClient.setUpDefaultClient();
		client.start();
		ConnectFuture connect;
		try {
			connect = client.connect(webspace, webspace + ".hostsharing.net", 22);
			connect.verify(2000L);
			session = connect.getSession();
			session.addPasswordIdentity(password);
			session.auth().verify(1000L);
		} catch (IOException e) {
			throw new WizException(e);
		}
	}
	
	public String execCommand(final String runAsUser, final String command) throws WizException {
		String commandOut = null;
		try {
			final StringBuilder commandBuilder = new StringBuilder();
			if (runAsUser != null && !runAsUser.isEmpty()) {
				commandBuilder.append("sudo -u ");
				commandBuilder.append(runAsUser);
				commandBuilder.append(" ");
			}
			commandBuilder.append("bash -l -c '");
			commandBuilder.append(command);
			commandBuilder.append("'");
			final String sshCommand = commandBuilder.toString();
			log.info("exec " + sshCommand);
			commandOut = session.executeRemoteCommand(sshCommand);
			return commandOut;
		} catch (IOException e) {
			throw new WizException(e);
		}
	}
	
	public String pushTemplate(final String runAsUser, final String template, final Map<String, Object> params, final String targetFile) throws WizException {
		String commandOut = null;
		String targetDir = null;
		if (targetFile == null || targetFile.isEmpty()) {
			throw new WizException("required parameter: targetfile");
		}
		if (template == null || template.isEmpty()) {
			throw new WizException("required parameter: template");
		}
		if (targetFile.contains("/")) {
			targetDir = targetFile.substring(0, targetFile.lastIndexOf('/'));
		}
		log.info("dir: " + targetDir);
		try {
			final StringBuilder commandBuilder = new StringBuilder();
			if (runAsUser != null && !runAsUser.isEmpty()) {
				commandBuilder.append("sudo -u ");
				commandBuilder.append(runAsUser);
				commandBuilder.append(" ");
			}
			commandBuilder.append("bash -s");
			final String sshCommand = commandBuilder.toString();
			log.info(sshCommand);
			final ChannelExec channel = session.createExecChannel(sshCommand);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			channel.setOut(out);
			channel.setErr(out);
			channel.open().verify(1000L);
			final OutputStream in = channel.getInvertedIn();
			final OutputStreamWriter writer = new OutputStreamWriter(in);
			writer.write("cd\n");
			if (targetDir != null && !targetDir.isEmpty()) {
				writer.write("mkdir -p " + targetDir + "\n");
			}
			writer.write("cat <<INHERE >" +  targetFile + "\n");
			final TemplateEngine engine = new TemplateEngine();
			engine.writeTemplate(template, params, writer);
			writer.write("\n");
			writer.write("INHERE\n");
			writer.close();
			channel.waitFor(EnumSet.of(ClientChannelEvent.EXIT_STATUS), TimeUnit.SECONDS.toMillis(20));
			commandOut = new String(out.toByteArray());
			return commandOut;
		} catch (IOException e) {
			throw new WizException(e);
		}
	}
	
	public String execTemplate(final String runAsUser, final String template, final Map<String, Object> params) throws WizException {
		String commandOut = null;
		if (template == null || template.isEmpty()) {
			throw new WizException("required parameter: template");
		}
		try {
			final StringBuilder commandBuilder = new StringBuilder();
			if (runAsUser != null && !runAsUser.isEmpty()) {
				commandBuilder.append("sudo -u ");
				commandBuilder.append(runAsUser);
				commandBuilder.append(" ");
			}
			commandBuilder.append("bash -s");
			final String sshCommand = commandBuilder.toString();
			log.info(sshCommand);
			final ChannelExec channel = session.createExecChannel(sshCommand);
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			channel.setOut(out);
			channel.setErr(out);
			channel.open().verify(1000L);
			final OutputStream in = channel.getInvertedIn();
			final OutputStreamWriter writer = new OutputStreamWriter(in);
			final TemplateEngine engine = new TemplateEngine();
			engine.writeTemplate(template, params, writer);
			writer.close();
			channel.waitFor(EnumSet.of(ClientChannelEvent.EXIT_STATUS), TimeUnit.SECONDS.toMillis(300));
			commandOut = new String(out.toByteArray());
		} catch (IOException e) {
			throw new WizException(e);
		}
		return commandOut;
	}
	
	public void close() throws WizException {
		try {
			session.close();
		} catch (IOException e) {
			throw new WizException(e);
		}
	}
	
	public static void main(String[] args) {
		try {
			final SshRemote sshRemote = new SshRemote(args[0], args[1]);
//			final String output = sshRemote.execCommand(args[2], "cd $HOME ; systemctl --user --no-pager status");
//			System.out.println(output);
			final HashMap<String, Object> params = new HashMap<>();
			params.put("pac", args[0]);
			params.put("user", args[2]);
			params.put("domain", args[3]);
			final String output1 = sshRemote.execTemplate(args[2], "install.sh.vm", params);
			System.out.println(output1);
			final String output2 = sshRemote.execTemplate(args[2], "domain.sh.vm", params);
			System.out.println(output2);
			sshRemote.close();
		} catch (WizException e) {
			System.out.println(e.getMessage());
		}
	}
}
