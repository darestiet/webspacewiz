package de.jalin.webspacewiz.conf;

import java.util.Map;

public class Installer {

	private String script;
	private Map<String, String> params;
	
	public String getScript() {
		return script;
	}
	
	public void setScript(String script) {
		this.script = script;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}
	
	@Override
	public String toString() {
		return "installer " + script;
	}
}
