package de.jalin.webspacewiz.conf;

import de.jalin.webspacewiz.RandomPasswordGenerator;

public class Application {

	private String name;
	private String password;
	private String comment;
	private Database database; 
	private Domain domain;
	private Installer installer;
	
	public Application() {
		password = RandomPasswordGenerator.randomPassword(29);
		comment = "Application";
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public Database getDatabase() {
		return database;
	}

	public void setDatabase(Database database) {
		this.database = database;
	}

	public Domain getDomain() {
		return domain;
	}

	public void setDomain(Domain domain) {
		this.domain = domain;
	}

	public Installer getInstaller() {
		return installer;
	}

	public void setInstaller(Installer installer) {
		this.installer = installer;
	}
	
}
