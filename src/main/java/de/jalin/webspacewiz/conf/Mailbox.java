package de.jalin.webspacewiz.conf;

import java.util.ArrayList;
import java.util.List;

import de.jalin.webspacewiz.RandomPasswordGenerator;

public class Mailbox {

	private String name;
	private String comment;
	private String password;
	private List<String> emailaddress;

	public Mailbox(final String name) {
		this.name = name;
		this.password = RandomPasswordGenerator.randomPassword(29);
		this.comment = "Mailbox";
		this.emailaddress = new ArrayList<>();
	}
	
	public Mailbox() {
		this.password = RandomPasswordGenerator.randomPassword(29);
		this.comment = "Mailbox";
		this.emailaddress = new ArrayList<>();
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public List<String> getEmailaddress() {
		return emailaddress;
	}

	public void setEmailaddress(List<String> emailaddress) {
		this.emailaddress = emailaddress;
	}
	
	public void addEMailAddress(String email) {
		this.emailaddress.add(email);
	}

	@Override
	public String toString() {
		final StringBuffer buf = new StringBuffer("mailbox ");
		buf.append(name);
		buf.append(" [ ");
		boolean firstloop = true;
		for (String singleAddress : emailaddress) {
			if (firstloop) {
				firstloop = false;
			} else {
				buf.append(", ");
			}
			buf.append(singleAddress);
		}
		buf.append("] ");
		return buf.toString();
	}
	
}
