package de.jalin.webspacewiz.conf;

import java.util.List;

public class YamlRoot {

	private String pac;
	private List<Application> applications;
	private List<Mailbox> mailboxes;
	
	public String getPac() {
		return pac;
	}
	
	public void setPac(String pac) {
		this.pac = pac;
	}

	public List<Application> getApplications() {
		return applications;
	}

	public void setApplications(List<Application> applications) {
		this.applications = applications;
	}
	
	public List<Mailbox> getMailboxes() {
		return mailboxes;
	}

	public void setMailboxes(List<Mailbox> mailboxes) {
		this.mailboxes = mailboxes;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("pac " + pac + " with apps ");
		boolean firstLoop = true;
		for (final Application app : applications) {
			if (firstLoop) {
				firstLoop = false;
			} else {
				sb.append(", ");
			}
			sb.append(app.getName());
		}
		sb.append("\n");
		for (final Mailbox mbox : mailboxes) {
			sb.append(mbox.toString());
			sb.append("\n");
		}
		return sb.toString();
	}
}
