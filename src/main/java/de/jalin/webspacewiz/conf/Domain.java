package de.jalin.webspacewiz.conf;

public class Domain {

	private String name;
	private boolean www;
	private String php;
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public boolean isWww() {
		return www;
	}

	public void setWww(boolean www) {
		this.www = www;
	}

	public String getPhp() {
		return php;
	}

	public void setPhp(String phpVersion) {
		this.php = phpVersion;
	}
	
	@Override
	public String toString() {
		return "domain " + name;
	}
}
