package de.jalin.webspacewiz;

public class WizException extends Exception {

	private static final long serialVersionUID = 1L;

	public WizException(final Throwable e) {
		super(e);
	}

	public WizException(String message) {
		super(message);
	}

}
