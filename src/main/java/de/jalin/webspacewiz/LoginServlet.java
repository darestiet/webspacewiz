package de.jalin.webspacewiz;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.jalin.webspacewiz.rpc.LoginSession;

public class LoginServlet extends HttpServlet {

	public static final String WIZARD_SESSION = "wiz_session";

	private static final long serialVersionUID = 1L;

	private static Log log = new Log("LoginServlet");

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("enter doGet()");
		final String redirectPath = req.getContextPath() + "/login.jsp";
		log.info("redirectTo:" + redirectPath);
		resp.sendRedirect(redirectPath);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		log.info("enter doPost()");
		final String user = req.getParameter("user");
		final String password = req.getParameter("password");
		log.info("name:" + user);
		log.info("password:" + password.substring(0, 2) + "****");
		final LoginSession wizSession = new LoginSession();
		wizSession.login(user, password);
		final HttpSession httpSession = req.getSession();
		httpSession.setAttribute(WIZARD_SESSION, wizSession);
		final String redirectPath = req.getContextPath() + "/index.jsp";
		log.info("redirectTo:" + redirectPath);
		resp.sendRedirect(redirectPath);
	}
}
