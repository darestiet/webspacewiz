package de.jalin.webspacewiz.rpc;

import java.io.IOException;

import de.jalin.webspacewiz.Log;

public class LoginSession {

	private static Log log = new Log("LoginSession");
	
	private TicketProvider ticketProvider;
	private String authorizedUser;
	private String password;

	public boolean isValid() {
		return ticketProvider != null && ticketProvider.isValid();
	}

	public void login(final String user, final String passwd) throws IOException {
		log.info("login(" + user + ")");
		ticketProvider = new TicketProvider(user, passwd);
		ticketProvider.getTicket();
		authorizedUser = user;
		password = passwd;
	}

	public String getAuthorizedUser() {
		return authorizedUser;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String nextTicket() throws IOException {
		log.info("next ticket");
		return ticketProvider.getTicket();
	}
	
}
