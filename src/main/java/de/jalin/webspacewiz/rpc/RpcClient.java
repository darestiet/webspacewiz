package de.jalin.webspacewiz.rpc;

import java.io.IOException;
import java.io.Serializable;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import de.jalin.webspacewiz.Log;
import de.jalin.webspacewiz.WizException;

public class RpcClient {

	private static final Log log = new Log("RpcClient");
	
	private final LoginSession loginSession;
	private XmlRpcClient xmlrpcClient;

	public RpcClient(final LoginSession session) {
		loginSession = session;
		final XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
		try {
			config.setServerURL(new URL("https://config.hostsharing.net:443/hsar/xmlrpc/hsadmin"));
			config.setEnabledForExtensions(true);
			xmlrpcClient = new XmlRpcClient();
			xmlrpcClient.setConfig(config);
		} catch (MalformedURLException e) {
			// should not happen
			xmlrpcClient = null;
			log.error(e.getMessage());
		}	
	}
	
	private Object rpcCall(final String module, final String method, final HashMap<String, Serializable> where, final HashMap<String, Serializable> set) throws WizException {
		final String methodName = module + "." + method;
		log.info("rpcCall(" + methodName + ")");
		final List<Serializable> xmlRpcParamsList = new ArrayList<Serializable>();
		try {
			xmlRpcParamsList.add(loginSession.getAuthorizedUser());
			xmlRpcParamsList.add(loginSession.nextTicket());
			if (method != null && ("search".equals(method) || "update".equals(method) || "delete".equals(method))) {
				xmlRpcParamsList.add(where);
			}
			if (method != null && ("add".equals(method) || "update".equals(method))) {
				xmlRpcParamsList.add(set);
			}
			final Object rpcResult = xmlrpcClient.execute(methodName, xmlRpcParamsList);
			return rpcResult;
		} catch (IOException | XmlRpcException e) {
			log.error(e.getMessage());
			throw new WizException(e);
		}
	}
	
	public Object add(final String module, final HashMap<String, Serializable> userAttribs) throws WizException {
		return rpcCall(module, "add", null, userAttribs);
	}

	public Object[] search(final String module) throws WizException {
		final HashMap<String, Serializable> query = new HashMap<String, Serializable>();
		return (Object[]) rpcCall(module, "search", query, null);
	}

	public Object[] search(final String module, final String objectName) throws WizException {
		return search(module, "name", objectName);
	}

	public Object[] search(final String module, final String key, final String value) throws WizException {
		final HashMap<String, Serializable> query = new HashMap<String, Serializable>();
		query.put(key, value);
		return (Object[]) rpcCall(module, "search", query, null);
	}

	public Object[] update(final String module, final String objectName, final HashMap<String, Serializable> attribsToChange) throws WizException {
		final HashMap<String, Serializable> query = new HashMap<String, Serializable>();
		query.put("name", objectName);
		return (Object[]) rpcCall(module, "update", attribsToChange, query);
	}

	public Object[] delete(final String module, final String objectName) throws WizException {
		final HashMap<String, Serializable> query = new HashMap<String, Serializable>();
		query.put("name", objectName);
		return (Object[]) rpcCall(module, "delete", query, null);
	}

}
