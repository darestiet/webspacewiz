package de.jalin.webspacewiz.rpc;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.net.URLEncoder;
import java.util.logging.Logger;

import javax.net.ssl.HttpsURLConnection;

public class TicketProvider {

	private static Logger log = Logger.getLogger("TicketProvider");

	private final String adminLogin;
	private final String adminPassword;
	
	private String grantingTicket = null;
	
	public TicketProvider(final String login, final String password) {
		this.adminLogin = login;
		this.adminPassword = password;
	}
	
	public boolean isValid() {
		return (grantingTicket != null) && grantingTicket.startsWith("https://login.hostsharing.net/cas/v1/tickets/TGT");
	}
	
	public String getAdminLogin() {
		return adminLogin;
	}
	
	public String getTicket() throws IOException {
		if (grantingTicket == null) {
			grantingTicket = getGrantingTicket();
			if (grantingTicket == null) {
				log.severe("login failed");
			} else {
				log.info("TGT:" + grantingTicket.substring(0, 60));
			}
		}
		String ticket = null;
		while (ticket == null && grantingTicket != null) {
			final String serviceParam = "service=" + URLEncoder.encode("https://config.hostsharing.net:443/hsar/backend", "UTF-8");
			final URL url = new URL(grantingTicket);
			final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);
			connection.setAllowUserInteraction(false);
			final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
			writer.write(serviceParam);
			writer.close();
			connection.connect();
			int httpResponseCode = connection.getResponseCode();
			if (200 == httpResponseCode) {
				final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
				ticket = reader.readLine();
				String readLine = reader.readLine();
				do {
					readLine = reader.readLine();
				} while (readLine != null);
			}
			if (httpResponseCode >= 400) {
				grantingTicket = getGrantingTicket();
			}
		}
		return ticket;
	}
	
	private String getGrantingTicket() throws IOException {
		final String userParam = "username=" + URLEncoder.encode(adminLogin, "UTF-8");
		final String passwordParam = "password=" + URLEncoder.encode(adminPassword, "UTF-8");
		final String encodedData = userParam + "&" + passwordParam;
		final URL url = new URL("https://login.hostsharing.net/cas/v1/tickets");
		final HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
		connection.setRequestMethod("POST");
		connection.setRequestProperty("Content-type", "application/x-www-form-urlencoded; charset=UTF-8");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
		connection.setAllowUserInteraction(false);
		final BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(connection.getOutputStream()));
		writer.write(encodedData);
		writer.close();
		connection.connect();
		return connection.getHeaderField("Location");
	}
	
}
