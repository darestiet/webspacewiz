package de.jalin.webspacewiz;

import java.io.FileInputStream;
import java.io.FileNotFoundException;

import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import de.jalin.webspacewiz.conf.YamlRoot;

public class Main {

	public static void main(String[] args) {
		try {
			final CommandlineParser parser = new CommandlineParser(args);
			final String pac = parser.getLogin();
			final String passwd = parser.getPassword();
			final String yamlFile = parser.getYaml();
			Yaml yaml = new Yaml(new Constructor(YamlRoot.class));
			Object object = yaml.load(new FileInputStream(yamlFile));
			System.out.println(object);
			System.out.println(pac + ":" + passwd);
		} catch (WizException | FileNotFoundException e) {
			System.err.println(e.getLocalizedMessage());
		}
	}
}
