package de.jalin.webspacewiz;

import java.io.Writer;
import java.util.Map;
import java.util.Properties;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;

public class TemplateEngine {

	final static Properties props = new Properties();
	
	static {
		props.put("resource.loaders", "class");
		props.put("resource.loader.class.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
	}

	final VelocityEngine velo;
	
	public TemplateEngine() {
		velo = new VelocityEngine(props);
	}
	
	public void writeTemplate(final String resourceName, final Map<String, Object> params, final Writer writer) {
		final Template template = velo.getTemplate(resourceName);
		final VelocityContext context = new VelocityContext(params);
		template.merge(context, writer);
	}

}
