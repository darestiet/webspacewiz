package de.jalin.webspacewiz;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;

public class CommandlineParser {

	final private CommandLine cmd;
	final private Options opts;

	public CommandlineParser(String[] args) throws WizException {
		opts = new Options();
		opts.addOption("h", "help", false, "print this message");
		opts.addOption("l", "login", true, "specify login user (pac admin)");
		opts.addOption("y", "yaml", true, "configuration to execute");
		opts.addOption("p", "password", true, "password");
		final DefaultParser parser = new DefaultParser();
		try {
			if (args.length < 1) {
				printHelp();
				System.exit(0);
			}
			cmd = parser.parse(opts, args);
			if (cmd.hasOption("help")) {
				printHelp();
				System.exit(0);
			}
		} catch (ParseException e) {
			throw new WizException(e); 
		}
	}
	
	public String getLogin() {
		return cmd.getOptionValue("login", null);
	}
	
	public String getPassword() {
		String password = cmd.getOptionValue("password", null);
		if (password == null || password.isEmpty()) {
			final LineReader reader = LineReaderBuilder.builder().build();
			password = reader.readLine("Password: ", Character.valueOf('*'));
		}
		return password;
	}
	
	public String getYaml() {
		return cmd.getOptionValue("yaml", null);
	}
	
	public void printHelp() {
		final HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp("webspacewiz", opts);
	}
	
	public String[] getArgs() {
		return cmd.getArgs();
	}

}
