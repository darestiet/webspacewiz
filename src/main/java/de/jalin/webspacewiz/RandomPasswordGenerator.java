package de.jalin.webspacewiz;

public class RandomPasswordGenerator {

	private final static String pwCharacters = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXYZ0123456789_$!#";
	private final static int length = pwCharacters.length() - 1;
	
	public static String randomPassword() {
		return randomPassword(18);
	}
		
	public static String randomPassword(int pwLength) {
		final StringBuffer buffer = new StringBuffer();
		for (int loop=0; loop<pwLength; loop++) {
			double rand = Math.random() * length;
			int idx = Math.round((float)rand);
			buffer.append(pwCharacters.charAt(idx));
		}
		return buffer.toString();
	}
	
}
