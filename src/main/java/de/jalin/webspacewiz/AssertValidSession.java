package de.jalin.webspacewiz;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.jalin.webspacewiz.rpc.LoginSession;


public class AssertValidSession extends HttpFilter {

	private static final long serialVersionUID = 1L;
	
	public static final String LOGIN_SERVLET_URL = "/login";
	public static final String LOGIN_JSP_URL = "/login.jsp";
	
	private static Log log = new Log("AssertValidSession");
	
	@Override
	public void doFilter(final HttpServletRequest request, final HttpServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		
		log.info("enter doFilter()");
		final HttpServletRequest httpRequest = (HttpServletRequest) request;
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		final String contextPath = httpRequest.getContextPath();
		final String requestURI = httpRequest.getRequestURI();
		log.info(requestURI);
		if (requestURI != null && (requestURI.equals(contextPath + LOGIN_JSP_URL) || requestURI.equals(contextPath + LOGIN_SERVLET_URL))) {
			log.info("allow login");
			chain.doFilter(request, response);
			return;
		}
		if (requestURI != null && (requestURI.startsWith(contextPath + "/webjars/") || requestURI.startsWith(contextPath + "/assets/"))) {
			log.info("allow assets");
			chain.doFilter(request, response);
			return;
		}
		final HttpSession httpSession = httpRequest.getSession();
		final Object wizObject = httpSession.getAttribute("wiz_session");
		if (wizObject != null && wizObject instanceof LoginSession) {
			final LoginSession wizSession = (LoginSession) wizObject;
			if (wizSession.isValid()) {
				chain.doFilter(request, response);
				return;
			}
		}
		log.info("sendRedirect()");
		httpResponse.sendRedirect(contextPath + LOGIN_SERVLET_URL);
	}

}
