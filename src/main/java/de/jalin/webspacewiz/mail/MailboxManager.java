package de.jalin.webspacewiz.mail;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import de.jalin.webspacewiz.WizException;
import de.jalin.webspacewiz.conf.Mailbox;
import de.jalin.webspacewiz.rpc.LoginSession;
import de.jalin.webspacewiz.rpc.RpcClient;

public class MailboxManager {

	final private RpcClient rpcClient;

	public MailboxManager(final RpcClient rpcClient) {
		this.rpcClient = rpcClient;
	}

	public Mailbox readMailbox(final String name) throws WizException {
		final Mailbox mbox = new Mailbox(name);
		final Object[] mboxObjects = rpcClient.search("user", name);
		if (mboxObjects.length == 1) {
			final Object obj = mboxObjects[0];
			if (obj != null && obj instanceof Map) {
				final Map<?, ?> map = (Map<?, ?>) obj;
				mbox.setComment((String) map.get("comment"));
				final Object[] addrObjects = rpcClient.search("emailaddress", "target", name);
				for (Object addr : addrObjects) {
					final Map<?, ?> addrMap = (Map<?, ?>) addr;
					mbox.addEMailAddress((String) addrMap.get("emailaddress"));
				}
				return mbox;
			}
		}
		throw new WizException("mailbox unkown");
	}
	
	public List<Mailbox> getMailboxes() throws WizException {
		final Map<String, Mailbox> allUsersMap = new HashMap<>();
		final Object[] userObjects = rpcClient.search("user");
		for (Object userObj : userObjects) {
			final Map<?, ?> userMap = (Map<?, ?>) userObj;
			final String name = (String) userMap.get("name");
			final String comment = (String) userMap.get("comment");
			final Mailbox mbox = new Mailbox(name);
			mbox.setComment(comment);
			allUsersMap.put(name, mbox);
		}
		final Object[] addrObjects = rpcClient.search("emailaddress");
		for (Object addrObj : addrObjects) {
			final Map<?, ?> addrMap = (Map<?, ?>) addrObj;
			final Object targetObj = addrMap.get("target");
			if (targetObj.getClass().isArray()) {
				final Object[] targetArray = (Object[]) targetObj;
				if (targetArray.length == 1) {
					final String target = (String) targetArray[0]; 
					final String emailAddr = (String) addrMap.get("emailaddress");
//					System.out.println(emailAddr + " --> " + target);
					final Mailbox mailbox = allUsersMap.get(target);
					if (mailbox != null) {
						mailbox.addEMailAddress(emailAddr);
					}
				}
			}
		}
		final Set<String> keys = allUsersMap.keySet();
		final List<Mailbox> mboxList = new ArrayList<>();
		for (final String key : keys) {
			final Mailbox mbox = allUsersMap.get(key);
			final List<String> addresses = mbox.getEmailaddress();
			if (addresses != null && addresses.size() > 0) {
				mboxList.add(mbox);
			}
		}
		return mboxList;
	}
	
	public static void main(String[] args) {
		final LoginSession session = new LoginSession();
		try {
			session.login(args[0], args[1]);
			final RpcClient client = new RpcClient(session);
			final MailboxManager manager = new MailboxManager(client);

			final Mailbox mailbox = manager.readMailbox("xyz00-infobox");
			System.out.println(mailbox.toString());
			
//			final List<Mailbox> mailboxes = manager.getMailboxes();
//			for (final Mailbox mbox : mailboxes) {
//				final List<String> eMailAddresses = mbox.getEMailAddresses();
//				final StringBuilder builder = new StringBuilder();
//				for (String addr : eMailAddresses) {
//					builder.append(addr);
//					builder.append(' ');
//				}
//				System.out.println(mbox.getName() + "( " + builder.toString() + ")");
//			}
		} catch (IOException | WizException e) {
			System.out.println(e.getMessage());
		}
	}
}
