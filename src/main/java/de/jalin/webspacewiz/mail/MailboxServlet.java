package de.jalin.webspacewiz.mail;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import de.jalin.webspacewiz.LoginServlet;
import de.jalin.webspacewiz.WizException;
import de.jalin.webspacewiz.conf.Mailbox;
import de.jalin.webspacewiz.rpc.LoginSession;
import de.jalin.webspacewiz.rpc.RpcClient;


public class MailboxServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		final String opCode = req.getParameter("op");
		final String servletPath = req.getServletPath();
		try {
			if ("/mailbox-list".equals(servletPath)) {
				final MailboxManager mngr = getMailboxManager(getRpcClient(req));
				final List<Mailbox> mailboxes = mngr.getMailboxes();
				final HttpSession session = req.getSession();
				session.setAttribute("mailboxes", mailboxes);
				req.getRequestDispatcher("/email.jsp").forward(req, resp);
				return;
			}
		} catch (WizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		final PrintWriter writer = resp.getWriter();
		String title = "<h4>Mailbox</h4>";
		if ("new".equals(opCode)) {
			title = "<h4>Mailbox anlegen</h4>";
		}
		if ("upd".equals(opCode)) {
			title = "<h4>Mailbox bearbeiten</h4>";
		}
		if ("del".equals(opCode)) {
			title = "<h4>Mailbox l&ouml;schen</h4>";
		}
		try {
			MailboxManager mngr = getMailboxManager(getRpcClient(req));
			Mailbox mbox = mngr.readMailbox("peh00-phor");
			title += "<p>" + mbox.getComment() + "</p>";
			List<String> addresses = mbox.getEmailaddress();
			for (String addr : addresses) {
				title += "<p>" + addr + "</p>";
			}
		} catch (WizException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		writer.write(title);
	}
	
	private RpcClient getRpcClient(final HttpServletRequest req) throws WizException {
		final HttpSession httpSession = req.getSession();
		final Object sessionObj = httpSession.getAttribute(LoginServlet.WIZARD_SESSION);
		if (sessionObj != null && sessionObj instanceof LoginSession) {
			final LoginSession loginSess = (LoginSession) sessionObj;
			final RpcClient rpcCLient = new RpcClient(loginSess);
			return rpcCLient;
		}
		throw new WizException("invalid session");
	}
	
	private MailboxManager getMailboxManager(final RpcClient rpcClient) {
		return new MailboxManager(rpcClient);
	}
}
