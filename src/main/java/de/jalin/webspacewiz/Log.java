package de.jalin.webspacewiz;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Log {

	final private Logger log;

	public Log(final String component) {
		log = LoggerFactory.getLogger(component);
	}
	
	public void info(final String info) {
		log.info(info);
	}
	
	public void warning(final String warning) {
		log.warn(warning);
	}
	
	public void error(final String error) {
		log.error(error);
	}
	
}
